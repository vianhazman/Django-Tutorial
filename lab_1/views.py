from django.shortcuts import render
import datetime

# Enter your name here
mhs_name = 'Favian Kharisma Hazman' # TODO Implement this
birth_year= 1998

# Create your views here.
def index(request):
    response = {'name': mhs_name,'age':calculate_age(birth_year)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    return datetime.date.today().year-birth_year

